import re

DirHorizontal = 0
Depth = 0
Aim = 0

with open(r"navfile.txt") as NavigationValues:
    for line in NavigationValues:
        dir = re.split(r'\s',line)        
        direction = dir[0]
        increment = int(dir[1])
        match direction:
            case 'forward':
                DirHorizontal += increment
                Depth += (Aim * increment)
            case 'down':                
                Aim += increment
            case 'up':
                Aim -= increment
                

print(f"Horizontal postion: {DirHorizontal}, Depth: {Depth}, Aim: {Aim} Multiplicated: {Depth*DirHorizontal}")
            