"""
import requests

data = requests.get(r"https://adventofcode.com/2021/day/1/input") -> needs login :(

print(*data)
"""
sonarfile = open(r"sonar_values.txt")
#sonardata = [int(line) for line in sonarfile]

trailing_data = 0
last_data = 0

runningwindow_last = -1

individual_count = -1
runningwindow_count = -3


linecount = 0

for line in sonarfile:
    data = int(line)
    if(data>last_data):
        individual_count += 1    

    runningwindow = (data + last_data + trailing_data) / 3
    if(runningwindow>runningwindow_last):
        runningwindow_count+=1

    trailing_data = last_data
    last_data = data
    runningwindow_last = runningwindow

    linecount += 1

print(f"Larger data count: {individual_count}, Larger Movingwindow count: {runningwindow_count}, lines {linecount}")
sonarfile.close