#MSb -> LSb
LineCt = 0
BinaryCtList = [0]*13
ValueList = []

def FilterStep(FilterList,offset,Least=False):
    Bincount = 0
    Valuect = 0
    for Value in FilterList:
        Bincount += 1 if (Value & (0x01<<offset)) > 0 else 0
        Valuect += 1
    MostCommonbit = bool(int(Bincount/(Valuect/2)))
    if Least == True:
        MostCommonbit = not MostCommonbit
    Filter = MostCommonbit<<offset
    FilterList = [element for element in FilterList if (element&(0x01<<offset) == Filter)]
    return FilterList

with open("binary.txt") as BinaryFile:
    for line in BinaryFile:
        ValueList.append(int(line,2))
        LineCt += 1
        for ct,char in enumerate(line):
            BinaryCtList[ct] += (1 if char == '1' else 0)
BinaryCtList.pop(12)            
GammaBinList = [int(val/(LineCt/2))*(2**(11-ct)) for ct,val in enumerate(BinaryCtList)] 
Gamma = sum(GammaBinList)
Epsilon = (Gamma^0xFFF)
print(bin(Gamma),bin(Epsilon),GammaBinList,BinaryCtList)
print(f"Gamma: {Gamma}, Epsilon: {Epsilon}, Multiplication: {Epsilon*Gamma} ")

FilterList_oxy = FilterList_co2sc = ValueList
Filter_oxy = Filter_co2osc = 0

for offset in reversed(range(12)):
    if(len(FilterList_oxy)>1):
        FilterList_oxy = FilterStep(FilterList_oxy,offset)
    if(len(FilterList_co2sc)>1):
        FilterList_co2sc = FilterStep(FilterList_co2sc,offset,True)
        

print(f"Oxygen generator rating: {FilterList_oxy[0]}, CO2 scrubber rating: {FilterList_co2sc[0]}\
    Multiplication: {FilterList_oxy[0]*FilterList_co2sc[0]} ")
